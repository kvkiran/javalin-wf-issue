package com.foo

import io.javalin.Context
import io.javalin.Javalin
import io.javalin.JavalinEvent

fun main(args: Array<String>) {
    val app = Javalin.create()
        .event(JavalinEvent.SERVER_STARTING) {}
        .event(JavalinEvent.SERVER_STARTED) {}
        .event(JavalinEvent.SERVER_START_FAILED) {}
        .event(JavalinEvent.SERVER_STOPPING) {}
        .event(JavalinEvent.SERVER_STOPPED) {}

    addExceptionHandles(app)
    app.start(7000)
    populateRoutes(app)
}

private fun addExceptionHandles(application: Javalin) {
    application.exception(Exception::class.java) { e, ctx ->
        ctx.status(500)
        ctx.result(e.message.toString())
    }

    application.error(500) {ctx ->
        ctx.result("Generic 500 message")
    }
}

private fun populateRoutes(application: Javalin) {
    application.get("") { ctx -> greet(ctx) }
    application.routes {

    }
}

private fun greet(ctx: Context) {
    println("Hello world")
//    throw Error()
    val foo = BadObject.foo
    println(foo)
    ctx.result(foo)
}